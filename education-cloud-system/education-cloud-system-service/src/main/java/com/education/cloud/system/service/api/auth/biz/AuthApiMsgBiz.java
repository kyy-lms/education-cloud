package com.education.cloud.system.service.api.auth.biz;

import com.education.cloud.system.common.bo.MsgViewBO;
import com.education.cloud.system.common.dto.MsgDTO;
import com.education.cloud.system.service.dao.MsgDao;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.tools.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.education.cloud.system.service.dao.impl.mapper.entity.Msg;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@Component
public class AuthApiMsgBiz {

	@Autowired
	private MsgDao dao;

	public Result<MsgDTO> view(MsgViewBO bo) {
		if (bo.getId() == null) {
			return Result.error("id不能为空");
		}
		Msg msg = dao.getById(bo.getId());
		if (ObjectUtils.isEmpty(msg)) {
			return Result.error("查询错误");
		}
		return Result.success(BeanUtil.copyProperties(msg, MsgDTO.class));
	}
}
