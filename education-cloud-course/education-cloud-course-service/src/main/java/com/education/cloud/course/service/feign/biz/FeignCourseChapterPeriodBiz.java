package com.education.cloud.course.service.feign.biz;

import com.education.cloud.course.feign.qo.CourseChapterPeriodQO;
import com.education.cloud.course.service.dao.CourseChapterPeriodDao;
import com.education.cloud.course.feign.vo.CourseChapterPeriodVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.education.cloud.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 课时信息
 *
 * @author wujing
 */
@Component
public class FeignCourseChapterPeriodBiz {

    @Autowired
    private CourseChapterPeriodDao dao;

    public Page<CourseChapterPeriodVO> listForPage(CourseChapterPeriodQO qo) {
        CourseChapterPeriodExample example = new CourseChapterPeriodExample();
        example.setOrderByClause(" id desc ");
        Page<CourseChapterPeriod> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, CourseChapterPeriodVO.class);
    }

    public int save(CourseChapterPeriodQO qo) {
        CourseChapterPeriod record = BeanUtil.copyProperties(qo, CourseChapterPeriod.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public CourseChapterPeriodVO getById(Long id) {
        CourseChapterPeriod record = dao.getById(id);
        return BeanUtil.copyProperties(record, CourseChapterPeriodVO.class);
    }

    public int updateById(CourseChapterPeriodQO qo) {
        CourseChapterPeriod record = BeanUtil.copyProperties(qo, CourseChapterPeriod.class);
        return dao.updateById(record);
    }

    /**
     * 根据视频编号查询课时正式表信息
     *
     * @param videoNo
     * @return
     * @author wuyun
     */
    public CourseChapterPeriodVO getByVideoNo(Long videoNo) {
        CourseChapterPeriod record = dao.getByVideoNo(videoNo);
        return BeanUtil.copyProperties(record, CourseChapterPeriodVO.class);
    }

}
