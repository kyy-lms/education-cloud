package com.education.cloud.course.service.feign;

import com.education.cloud.course.feign.interfaces.IFeignCourseChapter;
import com.education.cloud.course.feign.qo.CourseChapterQO;
import com.education.cloud.course.feign.vo.CourseChapterVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.education.cloud.course.service.feign.biz.FeignCourseChapterBiz;
import com.education.cloud.util.base.BaseController;
import com.education.cloud.util.base.Page;

/**
 * 章节信息
 *
 * @author wujing
 */
@Api(value = "章节信息", tags = "章节信息")
@RestController
public class FeignCourseChapterController extends BaseController implements IFeignCourseChapter {

    @Autowired
    private FeignCourseChapterBiz biz;

    @Override
    public Page<CourseChapterVO> listForPage(@RequestBody CourseChapterQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody CourseChapterQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody CourseChapterQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public CourseChapterVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

}
